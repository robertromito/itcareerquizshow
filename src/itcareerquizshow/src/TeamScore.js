import React from 'react';

export default function TeamScore(props) {
        let classNames = ["App-team"]
        const teamNum = props.teamNumber;
        if (props.active_team) classNames.push("App-team-active");
        if (props.just_scored) {
            classNames.push("blink");
            setTimeout(() => 
                document.getElementById("team" + teamNum).classList.remove("blink"),
                 8000);
        }
        return (
            <div id={"team" + teamNum}
                className={classNames.join(' ')}>
                <p>Team {teamNum}</p>
                <h1 id="team{teamNum}-score" className="App-score">
                    {props.score}
                </h1>
                <p>Points</p>
            </div>
        );
}