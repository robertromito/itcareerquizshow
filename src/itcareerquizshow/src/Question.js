import React, { Component } from 'react';
import './App.css';

class Question extends Component {
    constructor() {
        super();
        this.state = { answered: false }
    }
    checkAnswer(choice) {
        const multiplier = (this.props.answers.includes(choice)) ? 1 : -1;
        this.setState({ answered: true });
        this.props.handleAnswer(this.props.points * multiplier);
    }
    renderChoice(choice) {
        let answerFlagClassName = "App-choice";
        if (this.state.answered) {
            answerFlagClassName += (this.props.answers.includes(choice)) ? " App-choice-correct" : " App-choice-incorrect";
        }
        return (
            <button
                className={answerFlagClassName} disabled={this.state.answered}
                onClick={(e) => this.checkAnswer(choice)}>
                {choice}
            </button>
        )
    }
    render() {
        const questionNum = this.props.questionNumber;
        const answeredClassName = (this.state.answered) ? " App-question-answered" : "";
        return (
            <div id={"question" + questionNum}
                className={"App-question" + answeredClassName}>
                <h1 className="App-question-for">
                    Question {questionNum}: {this.props.points} points
                </h1>
                <h1 className="App-question-text">{this.props.question}</h1>
                <div className="App-choices">
                    {this.props.choices.map((choice) => this.renderChoice(choice))}
                </div>
                {this.state.answered ?
                    <div className="App-factoid">
                        {this.props.factoid}
                    </div> 
                    : null
                }
            </div>
        );
    }
}

export default Question;
