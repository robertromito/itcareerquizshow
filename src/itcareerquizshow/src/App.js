import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TeamScore from './TeamScore.js';
import Question from './Question.js';

class App extends Component {

  constructor() {
    super();
    this.state = {
      teams: Array(4).fill(0),
      scores: Array(4).fill(0),
      questions: require('./questions.json'),
      current_team: 0,
      current_question: 1,
      last_team_to_score: -1
    };
    this.scoreAnswer = this.scoreAnswer.bind(this);
    this.nextQuestion = this.nextQuestion.bind(this);
  }

  scoreAnswer(points) {
    const teams = this.state.teams.slice();
    const team_number = this.state.current_team;
    const team_that_gets_the_points = team_number;
    teams[team_that_gets_the_points] += points;
    this.setState({ teams: teams, last_team_to_score: team_that_gets_the_points });
  }

  nextQuestion() {
    const team_number = this.state.current_team;
    const question_number = this.state.current_question;
    const next_question = question_number + 1;
    this.setState({
      current_team: team_number + 1,
      current_question: next_question,
      last_team_to_score: -1
    });
    location.href = "#question" + next_question;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>Welcome to the IT Career Day Quiz Extravaganza</h1>
        </div>
        <div className="App-teams">
          {this.state.teams.map((team, index) =>
            <TeamScore teamNumber={index + 1}
              score={this.state.teams[index]}
              just_scored={this.state.last_team_to_score == index}
              active_team={(index == this.state.current_team)} />
          )}
        </div>
        <div className="App-questions">
          {this.state.questions.map((question, index) =>
            <Question {...question}
              handleAnswer={this.scoreAnswer} />
          )}
        </div>
        <div>
          <button className="App-next-button" onClick={this.nextQuestion}>
            Next =>
          </button>
        </div>
      </div>
    )
  }
}

export default App;
