IT Career Day Quiz Show
=====

This is my contribution to kids in grades 3 - 5 ish. It's meant to be a 30 minute talk about working in IT, with a focus on building software.

Presentation Format
-----

* Put the kids into groups of 3 or 4. The teacher can plan this part out ahead of time.
* I'll need to hook my laptop up to a projector to present the game and debugging excercise
* Presentation starts off with the announcement "It's time to play 'Who wants to work on computers for a living' "
* The first group will be presented with a multiple choice question. They can confer before giving their answer. If they get it right, they earn some points. If they get it wrong, they'll get a red X or something
* After the first question is answered, we can have a little discussion about it. 
* There may be a timer running in the program giving 5 minutes / question
* When the second group goes, we'll find the bug. If the 2nd group gets the answer right, a different team will get the points. If the 2nd group gets the answer wrong, they'll get the points anyway and another Team will lose the points.
* Hopefully it will be super obvious to them and we'll have to get in and "debug" the problem.

Question Ideas
-----

1. Which of these languages can you use to program to a computer?
  * French, Parsel Tounge, Javascript, C#
2. What is an important tool that you use to program a computer?
  * Text editor, Compiler, Roller blades, Glue stick
3. What is an important skill to have if you work with computers?
  * Problem solving, Love of learning, Math, Patience
4. How much will you work with other people in an IT job?
  * What's "other people?", Never, Only when I have to, All the time.

The Program
-----

To start, I'll build something in javascript using VS Code & Chrome to edit and run.

It will be a pretty simple SPA. Will probably use Angular or Ember to build the app. Still deciding. Might want to use React just for hot reloading. This might be the deciding factor. Need to be able to show debugging / fixing without restarting the game.

The app should be pretty simple. Main features are:

* Define Teams
* Show the score for each Team
* Show the number of red X's for each Team
* Present questions and 4 answers
* Ability to click on each answer to see if it's right
* Show a discussion timer that is reset with each new question

I'll probably use Chrome Dev Tools too to fix data on the fly when the scoring bug happens.

I think we should have some typo's in there too

Future Goals
-----

This project will evolve over time. It might be a fun one to give to multiple schools near by. 

Maybe developers in other cities and towns can use it to present to their local schools.

Dare to dream...